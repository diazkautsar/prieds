const express = require('express')
const router = express.Router()

const hospitalServiceRoutes = require('./hospitalServiceRoutes/index')
const visitorRoutes = require('./visitors/index')

router.get('/', (req, res, next) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.send('Welcome to Hospital Customer Queue Management API. Created by Diaz Kautsar')
})

router.use(hospitalServiceRoutes)
router.use(visitorRoutes)

module.exports = router
