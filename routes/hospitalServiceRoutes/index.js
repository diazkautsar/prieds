const express = require('express')
const HospitalServiceController = require('../../modules/hospitalServices/controllers/index')
const HospitalServicesMiddlewares = require('../../modules/hospitalServices/middlewares/index')

const router = express.Router()
const Controllers = new HospitalServiceController()
const middlewares = new HospitalServicesMiddlewares()

router.post('/services', middlewares.validateReqBodyAddHospitalServices, Controllers.addHospitalServices)
router.get('/services', Controllers.getHospitalServiceList)

module.exports = router
