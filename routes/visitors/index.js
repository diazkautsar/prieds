const express = require('express')

const VisitorControllers = require('../../modules/visitors/controllers')
const VisitorMiddlewares = require('../../modules/visitors/middlewares')

const router = express.Router()
const controllers = new VisitorControllers()
const middlewares = new VisitorMiddlewares()

router.post('/visitor', middlewares.validateReqBodyVisitors, controllers.addVisitor)
router.put('/visitor', middlewares.validateReqBodyUpdateVisitors, controllers.updateStatusVisitor)
router.get('/visitor', controllers.getVisitorList)

module.exports = router
