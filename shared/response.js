class Response {
  constructor(statusCode, status, message, data) {
    this.statusCode = statusCode || 500
    this.status = status
    this.message = message
    this.data = data
  }
}

module.exports = Response
