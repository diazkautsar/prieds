'use strict';
const joi = require('@hapi/joi');

const envVariablesSchema = joi.object({
  PORT: joi.string().required(),
  MONGODB_URI: joi.string().required(),
}).unknown().required();

module.exports = {
  envVariablesSchema,
}
