require('dotenv').config();

const mongoose = require('mongoose');

module.exports = {
  connect: () => {
    return new Promise((resolve, reject) => {
      mongoose.set('runValidators', true);
      mongoose.connect(process.env.MONGODB_URI)
        .then(() => {
          console.log('app connected to DB');
          return resolve('app connected to DB');
        });

      mongoose.connection.on('error', (err) => {
        console.log(`DB connection error: ${err.message}`);
        return reject(err);
      });
    });
  },
}
