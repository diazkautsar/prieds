# Hosptial Queue Management

## Requirement

- Node.js `>= 14.0.0`
- ExpressJs `>= 4.0.0`
- MongoDB

# Installation

1. Clone this repo to your localhost
2. Copy `.env-example` to `.env`. Insert value to `.env`
3. Install depedency
   ```bash
    npm install
   ```
4. Run script
   ```bash
   npm run dev
   ```
5. You are ready to go

##

# API ENDPOINT

1. Add Hospital Services

   ```bash
   method: POST
   url: /services
   body: {
    "title": string | required,
   }
   response: {
    201: {
      "statusCode": number,
       "status": string,
       "message": string,
       "data": {
         "slug": string,
         "code": string,
       }
    },
    40* / 50* : {
      "message": string | array
    }
   }
   ```

2. Get Hospital Services

   ```bash
   method: GET
   url: /services
   response: {
     200: {
        "statusCode": number,
        "status": string,
        "message": string,
        "ddata": [
          {
            "_id": string,
            "title": string,
            "slug": string,
            "code": string,
            "is_active": number,
            "created_at": string date,
            "__v": number
          }
        ]
     },
     40* / 50* {
        "message": string,
     }
   }
   ```

3. Add Visitors (print label queue number)

   ```bash
   method: POST
   url: /visitor
   body: {
      "first_name": string | required,
      "last_name": string | optional,
      "address": string | required,
      "services_id": string | required,
      "note": string | optional
   }
   response: {
     201: {
        "statusCode": number,
        "status": string,
        "message": string,
        "data": {
          "first_name": string,
          "last_name": string,
          "address": string,
          "note": string,
          "queue_number": number,
          "queue_code": string,
          "status": string,
          "services_id": string,
          "created_at": string | date,
          "_id": string,
          "__v": 0
        }
     }
     40* / 50* {
        "message": string,
     }
   }
   ```

4. Update status visitors

   ```bash
   method: PUT
   url: /visitor
   body: {
     "id": string | required | visitor id,
     "status": string | required | enum (waiting, on_check, done)
   }
   response: {
     200: {
       "message": string,
     }
     40* / 50* {
       "message": string,
     }
   }
   ```

5. Get Visitors
   ```bash
   method: GET
   url: /visitor
   param: {
     "status": string | optional | enum (waiting, on_check, done)
   },
   response: {
     200: {
        "statusCode": number,
        "status": string,
        "message": string,
        "data": [
          {
            "_id": string,
            "first_name": string,
            "last_name": string,
            "address": string,
            "note": string,
            "queue_number": number,
            "queue_code": string,
            "status": string,
            "services_id": string,
            "created_at": string,
            "__v": number
          }
        ]
     }
     40* / 50* {
        "message": string,
     }
   }
   ```
