class HospitalServiceUtils {
  createSlugService(param) {
    return param.toLowerCase().split(' ').join('-')
  }

  createCodeService(param) {
    return param.toUpperCase().split(' ').map(item => item[0]).join('')
  }
}

module.exports = HospitalServiceUtils
