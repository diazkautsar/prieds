const joi = require('@hapi/joi')

const reqBodyAddHospitalServices = joi.object({
  title: joi.string().min(1).required().messages({
    'string.empty': 'Display title cannot be empty',
    'string.min': 'Min 1 characteers',
  }),
});

class HospitalServiceMiddlewares {
  constructor() {
    
  }

  async validateReqBodyAddHospitalServices(req, res, next) {
    const { error, value } = reqBodyAddHospitalServices.validate(req.body);

    if (error) {
      res.status(400).json({
        message: error.details.map(item => item.message),
      });
    };

    next();
  };
};

module.exports = HospitalServiceMiddlewares
