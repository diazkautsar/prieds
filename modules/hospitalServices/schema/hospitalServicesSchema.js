const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const HospitalServicesSchema = new Schema({
  title: { type: String, required: true, unique: true },
  slug: { type: String, required: true },
  code: { type: String, required: true, unique: true },
  is_active: { type: Number, enum: [1, 0], default: 1, required: true },
  created_at: { type: Date, default: Date.now() },
  updated_at: { type: Date }
})

module.exports = mongoose.model('HospitalServices', HospitalServicesSchema)

