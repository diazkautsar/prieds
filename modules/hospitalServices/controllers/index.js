const HospitalServicesUtils = require('../utils/index')
const Response = require('../../../shared/response')

const HospitalSchema = require('../schema/hospitalServicesSchema');

const utils = new HospitalServicesUtils()

class HospitalServiceControllers {
  async addHospitalServices(req, res, next) {
    try {
      const slug = utils.createSlugService(req.body.title)
      const code = req.body.code ? req.body.code : utils.createCodeService(req.body.title)

      await HospitalSchema.create({
        title: req.body.title,
        slug,
        code,
        is_active: 1
      })

      const response = new Response(201, 'ok', `success create services ${req.body.title}`, { slug, code })

      res.status(201).json({
        ...response,
      });

    } catch (error) {
      next({
        message: error && error.message ? error.message : 'Internal Server Error',
      });
    };
  };

  async getHospitalServiceList(req, res, next) {
    try {
      const services = await HospitalSchema.find({ is_active: 1 }).exec()

      const response = new Response(200, 'ok', 'success fetched hospital services', services);
      res.status(201).json({
        ...response,
      });
    } catch (error) {
      next({
        message: error && error.message ? error.message : 'Internal Server Error',
      });
    };
  };
}

module.exports = HospitalServiceControllers
