const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const VisitorsSchema = new Schema({
  first_name: { type: String, required: true },
  last_name: { type: String },
  address: { type: String, required: true },
  note: { type: String },
  queue_number: { type: Number, required: true },
  queue_code: { type: String, required: true, unique: true },
  status: { type: String, enum: [ 'waiting', 'on_check', 'done' ], required: true },
  services_id: { type: Schema.Types.ObjectId, ref: 'HospitalServices' },
  created_at: { type: Date, default: Date.now() },
  updated_at: { type: Date },
})

module.exports = mongoose.model('Visitors', VisitorsSchema)
