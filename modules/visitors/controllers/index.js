const VisitorSchema = require('../schema/visitorSchema')
const hospitalServicesSchema = require('../../hospitalServices/schema/hospitalServicesSchema')

const Response = require('../../../shared/response')
const VisitorUtils = require('../utils/index')

const utils = new VisitorUtils()

class VisitorControllers {
  async addVisitor(req, res, next) {
    try {
      const { first_name, last_name, address, note, services_id, status } = req.body;

      const service = await hospitalServicesSchema.findById(services_id).exec();

      const queue_number = await utils.getQueueNumber(services_id);

      const data = await VisitorSchema.create({
        first_name,
        last_name,
        address,
        note,
        services_id,
        queue_number,
        queue_code: service.code + queue_number,
        status: status ? status : 'waiting',
      });

      const response = new Response(201, 'ok', `success add visitor: ${first_name} - ${last_name}`, data);

      res.status(201).json({
        ...response,
      });
    } catch (error) {
      next({
        message: error && error.message ? error.message : 'Internal Server Error',
      });
    } 
  }

  async updateStatusVisitor(req, res, next) {
    try {
      const { id, status } = req.body
      await VisitorSchema.findOneAndUpdate({ _id: id }, { status })
      const response = new Response(200, 'ok', `success update visitors`, []);
      res.status(200).json({
        ...response
      })
    } catch (error) {
      next({
        message: error && error.message ? error.message : 'Internal Server Error',
      });
    }
  }

  async getVisitorList(req, res, next) {
    try {
      const { status } = req.query

      let data;

      if (status) {
        data = await utils.getAllVisitorBasedOnStatus(status)
      } else {
        data = await utils.getAllVisitor()
      }

      const response = new Response(200, 'ok', `success update visitors`, data);
      res.status(200).json({
        ...response
      })
    } catch (error) {
      next({
        message: error && error.message ? error.message : 'Internal Server Error',
      });
    }
  }
}

module.exports = VisitorControllers
