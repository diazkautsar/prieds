const visitorSchema = require('../schema/visitorSchema')

class VisitorUtils {
  async getQueueNumber(services_id) {
    const lastInserted = await visitorSchema.find({ services_id }).sort({ _id: -1 }).limit(1)
    if (!lastInserted.length) {
      return 1
    }
    
    const data = lastInserted[0]

    return data.queue_number + 1
  }

  async getAllVisitor() {
    return await visitorSchema.find()
  }

  async getAllVisitorBasedOnStatus(status) {
    return await visitorSchema.find({ status })
  }
}

module.exports = VisitorUtils
