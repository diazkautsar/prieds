const joi = require('@hapi/joi')

const reqBodyAddVisitors = joi.object({
  first_name: joi.string().required(),
  last_name: joi.string(),
  address: joi.string().required(),
  note: joi.string(),
  services_id: joi.string().required(),
})

const reqBodyUpdateVisitors = joi.object({
  id: joi.string().required(),
  status: joi.string().valid('waiting', 'on_check', 'done'),
})

class VisitorMiddlewares {
  async validateReqBodyVisitors(req, res, next) {
    const { error, value } = reqBodyAddVisitors.validate(req.body)

    if (error) {
      res.status(400).json({
        message: error.details.map(item => item.message),
      });
    };

    next();
  }

  async validateReqBodyUpdateVisitors(req, res, next) {
    const { error } = reqBodyUpdateVisitors.validate(req.body)

    if (error) {
      res.status(400).json({
        message: error.details.map(item => item.message),
      });
    };

    next();
  }
}

module.exports = VisitorMiddlewares
