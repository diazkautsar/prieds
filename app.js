require('dotenv').config();

const { envVariablesSchema } = require('./config/env');
const routers = require('./routes')

const { error: envConfigError, value: envValue } = envVariablesSchema.validate(process.env);

if (envConfigError) {
  throw new Error(`env validation error ${envConfigError}`);
}

const express = require('express');
const cors = require('cors');
const logger = require('morgan');

const app = express();

app.use(logger('dev'));
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(routers);

app.use((err, req, res, next) => {
  res.status(err.statusCode || 500).json({
    message: err.message,
  })
})

module.exports = app
